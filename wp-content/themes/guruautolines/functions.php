<?php
function theme_enqueue_styles() {
    // wp_enqueue_style( 'guruautolines-parent-stylesheet', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'guruautolines-parent-stylesheet', get_stylesheet_directory_uri() . '/style.css' );        
    wp_enqueue_script( 'customjs', get_stylesheet_directory_uri() . '/customjs.js', array(), '1.0.0', true );
  
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );